# CircuitPlayground Virtual Candle

Simulated Candle-flickering code for the CircuitPlayground Express, suitable for use inside of a Jack O' Lantern or other place where you want a flickering, candle-style light (but for things to not be on fire).

## Theory

Multi-Color LEDs, such as the NeoPixels included in the CPE, are made from three tinier Red, Green, and Blue elements - Thus you get the most output when they're all set to maximum (which produces White-ish light).

To get a more authentic "flicker" candle effect, though, it helps to have that light "flicker" to different levels of brightness. To improve the candle effect, I prefer to lower the Blue and Green elements only, bringing the light "down" from White to Yellow or Orange, staying within the range of colors that candle flames tend to possess (and that don't look out of place inside pumpkins and Jack 'o Lanterns.)

Real candle flames can also change shape and "move around" to a limited degree when they flicker. Since the CPE has ten LEDs arranged in a rough circle, I got a more realistic, assymetrical flicker by having only a portion of the LEDs dim out to yellow or orange at at time.

Essentially, we have a set of "gradients", ranging from tiny (one LED wide) to XL (six LEDs wide), with different lengths, where the affected lights go from White, to Yellow, to Orange. We then pick a random gradient, and play it at a random speed, placing it random along the length of the LED array (wrapping around at the "edges", so a gradient can appear anywhere along the "ring" of lights.)

You may want to adjust the speed, as well as the initial (white) and flicker (orange/yellow) colors to match your preferences or use case.

Additional notes about how LEDs work, getting the colors you actually want, and using them inside pumpkins in this [Slide Deck](https://docs.google.com/presentation/d/1367gwgHFdclG1jAlz03YrqmgdsvduW_ntT89vimEuAQ/edit?usp=sharing), from slides 4 - 22.

## Installation

Copy `main.py` onto your CircuitPlayground Express, overwriting the one that may already be present. (If you have existing code on your CPE, this may be a good time to back it up on a computer or put it into version control!)

This code currently depends on [the FancyLED library](https://learn.adafruit.com/fancyled-library-for-circuitpython) - It has installation instructions, but TLDR you'll want to install the `adafruit_fancyled` folder inside a `lib` folder alongside the main.py.

It should start running immediately, or display some kind of error in the terminal if not. (see Development for more information on debugging...)

## Development

While the Mu editor isn't much to write home about, it makes it easy to edit code directly "onto" a CircuitPlayground Express, and the Serial/Terminal functionality is great for debugging code, and controllign the CPE "live" from a Python REPL.

https://codewith.mu/

I have heard there's an awesome VSCode Plugin now as well, but haven't had a chance to try it yet.

## Deployment

I also strongly recommend putting your CPE and battery pack into a sealable clear plastic bag, before putting it into a real pumpkin.

In other uses, you may want to cover the LEDs with a thin sheet of paper, or some other translucent surface that diffuses the light, and makes it look more like a single glowing ring or circle (and less like a bunch of dots), or install it such that the LED light is reflected towards viewers, rather than directly visible.

The [CircuitPlayground Express Base Kit](https://www.adafruit.com/product/3517) includes both the CPE itself, a USB cable for installing code and debugging, and a battery case that allows it to run off of three AAA batteries, once the code is loaded. (There are smaller, rechargable batteries available, but they tend to involve more fiddly, manual charging processes, and are potentially "explody" or otherwise dangerous, while AAA batteries are ubiquitous and comparatively safe.)
