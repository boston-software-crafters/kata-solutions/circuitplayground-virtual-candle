from adafruit_circuitplayground.express import cpx
import adafruit_fancyled.adafruit_fancyled as fancy
import time
import random


cpx.pixels.auto_write = False  # Do not auto-flush LED color data
cpx.pixels.brightness = 1.0    # Max brightness (Handle gamma via FancyLED)


# Adjusted for blue/white LEDs
# (Account for the natural color balance of LEDs)
# (plus element colors not being equally bright)
adjusted_levels = (0.25, 0.3, 0.15)


def accurate_color(red, green, blue):
    """
    Process RGB triplets into gamma-adjusted, packed color numbers
    (for more accurate colors, and more efficient communication with NeoPixels)
    """
    color = fancy.CRGB(red, green, blue)
    color = fancy.gamma_adjust(color, brightness=adjusted_levels)
    # Returns a single packed integer
    # e.g. (255, 0, 0) becomes 4194304
    return color.pack()


# Define (adjusted) color constants
WHITE =  accurate_color(255, 255, 255)
YELLOW = accurate_color(255, 255, 0)
ORANGE = accurate_color(255, 128, 0)
RED =    accurate_color(255, 0,   0)
OFF = (0, 0, 0)


# Constants for addressing the 10 NeoPixel LEDs by index
MAX_INDEX = 9
LIGHT_LENGTH = 10


# Initial light state (all 10 lights on full White)
empty_lights = [WHITE] * 10


# Gradient animations of various "widths" and durations
# Consistent width, and always ending in WHITE, so that
# the LEDs are left "clean" at the end

tiny1 = (
    (YELLOW,),
    (WHITE,),
)


tiny2 = (
    (YELLOW,),
    (YELLOW,),
    (WHITE,),
)


tiny3 = (
    (YELLOW,),
    (YELLOW,),
    (ORANGE,),
    (YELLOW,),
    (WHITE,),
)


small1 = (
    (YELLOW,),
    (ORANGE,),
    (YELLOW,),
    (WHITE,),
)


small2 = (
    (YELLOW,),
    (YELLOW,),
    (ORANGE,),
    (YELLOW,),
    (WHITE,),
)


small3 = (
    (YELLOW,),
    (ORANGE,),
    (RED,),
    (ORANGE,),
    (YELLOW,),
    (WHITE,),
)


medium = (
    (WHITE, YELLOW, WHITE),
    (YELLOW, ORANGE, YELLOW),
    (ORANGE, RED, ORANGE),
    (WHITE, ORANGE, YELLOW),
    (WHITE, YELLOW, WHITE),
    (WHITE, WHITE, WHITE),
)


large = (
    (WHITE, WHITE, YELLOW, WHITE, WHITE),
    (WHITE, YELLOW, YELLOW, YELLOW, WHITE),
    (WHITE, YELLOW, ORANGE, YELLOW, WHITE),
    (YELLOW, ORANGE, ORANGE, YELLOW, YELLOW),
    (YELLOW, ORANGE, ORANGE, ORANGE, YELLOW),
    (ORANGE, RED, RED, RED, ORANGE),
    (YELLOW, ORANGE, RED, ORANGE, YELLOW),
    (YELLOW, ORANGE, ORANGE, YELLOW, WHITE),
    (WHITE, YELLOW, ORANGE, YELLOW, WHITE),
    (WHITE, WHITE, YELLOW, WHITE, WHITE),
    (WHITE, WHITE, WHITE, WHITE, WHITE),
)


xl = (
    (WHITE, WHITE, YELLOW, WHITE, WHITE, WHITE),
    (WHITE, YELLOW, YELLOW, YELLOW, WHITE, WHITE),
    (WHITE, YELLOW, ORANGE, YELLOW, YELLOW, WHITE),
    (YELLOW, ORANGE, ORANGE, ORANGE, YELLOW, YELLOW),
    (YELLOW, ORANGE, RED, RED, ORANGE, YELLOW),
    (ORANGE, RED, RED, RED, RED, ORANGE),
    (YELLOW, ORANGE, ORANGE, RED, ORANGE, YELLOW),
    (YELLOW, YELLOW, ORANGE, ORANGE, YELLOW, YELLOW),
    (YELLOW, YELLOW, ORANGE, YELLOW, YELLOW, YELLOW),
    (WHITE, YELLOW, YELLOW, ORANGE, YELLOW, WHITE),
    (WHITE, WHITE, YELLOW, YELLOW, WHITE, WHITE),
    (WHITE, WHITE, YELLOW, WHITE, WHITE, WHITE),
    (WHITE, WHITE, WHITE, WHITE, WHITE, WHITE),
)


# Various amounts of delay (between gradient animation "frames")
delays = (0.01, 0.015, 0.02)


# Collection of all gradients
# (and thus their frequency of appearance)
animations = (
    tiny1, tiny2, tiny3,
    small1, small2, small3,
    medium, medium,
    large, large,
    xl,
)


# renders a gradient animation into the LED array
# (also returns the new LED color settings for reference)
def render_colors_to_leds(lights, gradient, position):
    #new_light_colors = list(lights)
    for i, g in enumerate(gradient):
        new_position = position + i
        # Wrap around if we go off the "end" of the list of LEDs
        new_position = (
            new_position - MAX_INDEX - 1
            if new_position > MAX_INDEX
            else new_position
        )
        #new_light_colors[new_position] = g
        cpx.pixels[new_position] = g
    return cpx.pixels


def fill_all_leds(single_color):
    # Set all LEDs to the same color:
    for np_index in range(MAX_INDEX):
        cpx.pixels[np_index] = single_color
    cpx.pixels.show()


fill_all_leds(WHITE)


# Initialize an empty (white) LED state
lights = list(empty_lights)


# Candle Simulator Loop
while True:
    # Pick a random animation width/length
    animation = random.choice(animations)

    # Place it randomly "around the ring"
    position = random.randrange(0, MAX_INDEX)

    # Choose a random animation speed
    animation_delay = random.choice(delays)

    # Display each step in the animation
    for animation_step in animation:
        lights = render_colors_to_leds(lights, animation_step, position)
        # Update state of physical LEDs from the cpx.pixels object
        cpx.pixels.show()
        #print(lights)
        time.sleep(animation_delay)

    # Wait a little between animations
    time.sleep(0.1)
